$( document ).ready(function() {
  //проверка подключения
  console.log( "hallo!" );


    // слайдер

    $('.slider').slick({
      dots: true,
      infinite: true,
      slidesToScroll: 1,
      autoplay: true,
      speed: 500,
      autoplaySpeed: 5000
  });


  //прокрутка

  $("a.anchor").click(function() {
    $("html, body").animate({
      scrollTop: $($(this).attr("href")).offset().top + "px"
    }, {
      duration: 1000,
      easing: "swing"
    });
    return false;
  });


  // меню в модалке
  $(window).resize();
  menu();

  function menu() {
    var winWidth = screen.width;
    if (winWidth < 414) {
      $(".nav").hide();
      $(".nav").click(function () {
        $(this).fadeOut();
      });
      $(".nav a").click(function () {
        $(".nav").fadeOut();
      });
      $(".menu").click(function () {
        $(".nav").fadeIn();
      });
      console.log(winWidth);
    }
    else {
      $(".nav").fadeIn();
      $(".menu").fadeOut();
      console.log(winWidth+'big');
    }
  };

  $( window ).resize(function() {
    menu();
  });

});